# Wunder Mobility Registeration: Challenge

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).  
Use redux as state management  
@testing-library/react is used as the test framework.  
Sass as styling  

### Quickstart
    needs node version > 12
    
    npm install
    npm start

### API call:
The api call "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiti" was returning CORS issues in the browser.  
I received the paymentID by calling api from another place, and use it in a json file, so I can fetch it locally.

### Optimizations
  1- Better to have a super class for registration views, then all components in RegistrationSteps section, extend that super class  
  2- Maybe it was better to not to have each view in seperate rout path.  
  3- In sass files, Use nestings  
  4- Have more validations controll in inputs (like check patter for IBAN, zipcode, ...)  
  
 I've made only one test case. It obviously needs more tests(unit tests, integration tests for components, end-to-end test=like cypress) for whole application) but I was not able to spend more time on it.