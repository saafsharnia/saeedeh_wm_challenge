import {createStore, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';
import combinedReducer from './reducers/index';

function configureStore(){
    return createStore(
        combinedReducer, applyMiddleware(thunkMiddleware)
    )
}

const store = configureStore();

export default store;