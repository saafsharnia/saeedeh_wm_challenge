import React, { Component } from 'react';
import './App.scss';
import Main from './components/Registration/Main';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import AddressInfo from './components/RegistrationSteps/AddressInfo';
import PaymentInfo from './components/RegistrationSteps/PaymentInfo';
import SuccessPage from './components/RegistrationSteps/SuccessPage';
import PersonalInfo from './components/RegistrationSteps/PersonalInfo';


class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          {/* <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2> */}
          <img src="https://d33wubrfki0l68.cloudfront.net/f2de54c7df6bfbd4a611c6280b67e4900f593f5a/c3ce9/uploads/global/wunder-logo-2020.svg" 
            alt="Wunder Mobility" 
            className="App-logo"
          />
        </div>
        <div className="card">
          <Router>
            <Switch>
              <Route exact path='/' component={Main} />
              <Route path='/personal_info' component={PersonalInfo} />
              <Route path='/address_info' component={AddressInfo} />
              <Route path='/payment_info' component={PaymentInfo} />
              <Route path='/success_page' component={SuccessPage} />
            </Switch>
          </Router>
        </div>
      </div>
    );
  }
}

export default App;
