import React, { Component } from 'react';
import '../../assets/styles/button.scss';

class Main extends Component {
  render() {
    return (
      <div>
        <h2>
          Main Page
        </h2>
        <button
          onClick={() => this.props.history.push('/personal_info')}
        >
          Start Registration
        </button>
        <button 
          onClick={() => this.props.history.goBack()}
        >
          Continue registration
        </button>
      </div>
    )
  }
}

export default Main;