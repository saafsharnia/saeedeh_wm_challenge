import React, { Component } from 'react';
import { setdataForm } from '../../actions/dataFormAction';
import { connect } from 'react-redux';
import store from '../../store';

class AddressInfo extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e, key) {
    store.dispatch(setdataForm(key, e.target.value))
  }

  handleSubmit(e) {
    const { street, houseNumber, zipCode, city } = this.props.dataForm;
    e.preventDefault()
    if(street && houseNumber && zipCode && city) {
      this.props.history.push('/payment_info')
    } else {
      alert("Please fill all the fields")
    }
  }

  render() {
    const dataForm = this.props.dataForm;

    return (
      <div>
        <h2>
          Address Information
        </h2>
        <form onSubmit={this.handleSubmit}>
          <label>Street:</label>
          <input className="text-input"
            value={dataForm.street}
            onChange={e => this.handleChange(e, 'street')}
          />
          <label>House number:</label>
          <input
            value={dataForm.houseNumber}
            onChange={e => this.handleChange(e, 'houseNumber')}
            type={"number"}
            className="text-input"
          />
          <label>Zip code:</label>
          <input
            value={dataForm.zipCode}
            onChange={e => this.handleChange(e, 'zipCode')}
            type={"number"}
            className="text-input"
          />
          <label>City:</label>
          <input
            value={dataForm.city}
            onChange={e => this.handleChange(e, 'city')}
            className="text-input"
          />
          <button
            className={"right-button"}
            type={"submit"}
          >
            next
          </button>
          <button
            className={"left-button"}
            onClick={() => this.props.history.push('/')}
          >
            Main page
          </button>
        </form>
      </div>
    )
  }
}

export default connect(state => ({ dataForm: state.dataForm }))(AddressInfo)
