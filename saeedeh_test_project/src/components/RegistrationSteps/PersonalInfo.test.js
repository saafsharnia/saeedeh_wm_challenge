import {PersonalInfoComponent} from './PersonalInfo';
import { render, fireEvent, queryByAttribute } from '@testing-library/react';
// import { render, queryByAttribute } from 'react-testing-library';
import React from 'react';


it('renders name input', () => {
  const dataForm = {
    firstName: 'Saeedeh',
    lastName: '',
    phoneNumber: '',
    street: '',
    houseNumber: '',
    zipCode: '',
    city: '',
    accountOwner: '',
    iban: '',
    paymentDataId: null
  }
  const getById = queryByAttribute.bind(null, 'id');
  const personalInfo = render(<PersonalInfoComponent dataForm={dataForm}/>);
  
  const nameInput = getById(personalInfo.container, 'name');
  expect(nameInput.value).toBe('Saeedeh')
  
});