import React, { Component } from 'react';
import { connect } from 'react-redux';

class SuccessPage extends Component {
  render() {
    const paymentDataId = this.props.dataForm.paymentDataId
    console.log(this.props.dataForm)

    return (
      <div>
        <h2>
          Sucessfully Registered
        </h2>
        <form>
          <h3>Payment Data id:</h3>
          <input
            defaultValue={paymentDataId}
            disabled
            className="text-input"
          />
          <button
            className={"right-button"}
            onClick={() => this.props.history.push('/')}
          >
            Main page
          </button>
        </form>
      </div>
    )
  }
}


export default connect(state => ({ dataForm: state.dataForm }))(SuccessPage)