import React, { Component } from 'react';
import '../../assets/styles/input.scss';
import { setdataForm } from '../../actions/dataFormAction';
import { connect } from 'react-redux';
import store from '../../store';

class PersonalInfo extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e, key) {
    store.dispatch(setdataForm(key, e.target.value))
  }

  handleSubmit(e) {
    const { firstName, lastName, phoneNumber } = this.props.dataForm;
    e.preventDefault()
    if(firstName && lastName && phoneNumber) {
      this.props.history.push('/address_info')
    } else {
      alert("Please fill all the fields")
    }
  }

  render() {
    const dataForm = this.props.dataForm
    console.log('dataForm', dataForm)

    return (
      <div>
        <h2>
          Personal Information
        </h2>
        <form onSubmit={this.handleSubmit}>
          <label>First Name:</label>
          <input className="text-input"
            id="name"
            value={dataForm.firstName}
            onChange={(e) => this.handleChange(e,'firstName')}
          />
          <label>Last Name:</label>
          <input
            id="last-name"
            value={dataForm.lastName}
            onChange={(e) => this.handleChange(e,'lastName')}
            className="text-input"
          />
          <label>Phone:</label>
          <input
            id="phone"
            value={dataForm.phoneNumber}
            onChange={(e) => this.handleChange(e,'phoneNumber')}
            type={"tel"}
            className="text-input"
          />
          <button
            className={"right-button"}
            type={"submit"}
          >
            next
          </button>
          <button
            className={"left-button"}
            onClick={() => this.props.history.push('/')}
          >
            Main page
          </button>
        </form>
      </div>
    )
  }
}

export const PersonalInfoComponent = PersonalInfo;

export default connect(state => ({ dataForm: state.dataForm }))(PersonalInfo)