import React, { Component } from 'react';
import { setdataForm, submitData } from '../../actions/dataFormAction';
import { connect } from 'react-redux';
import store from '../../store';

class PaymentInfo extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e, key) {
    store.dispatch(setdataForm(key, e.target.value))
  }

  handleSubmit(e) {
    const { accountOwner, iban } = this.props.dataForm;
    e.preventDefault()
    if(accountOwner && iban) {
      store.dispatch(submitData({
        "customerId": 1,
        "iban": iban,
        "owner": accountOwner
      }));
      localStorage.setItem('WM_dataForm', JSON.stringify(this.props.dataForm));
      this.props.history.push('/success_page')
    } else {
      alert("Please fill all the fields")
    }
  }

  render() {
    const dataForm = this.props.dataForm;
    console.log(dataForm)

    return (
      <div>
        <h2>
          Payment Information
        </h2>
        <form onSubmit={this.handleSubmit}>
          <label>Acount owner:</label>
          <input className="text-input"
            value={dataForm.accountOwner}
            onChange={e => this.handleChange(e, 'accountOwner')}
          />
          <label>IBAN:</label>
          <input
            value={dataForm.iban}
            onChange={e => this.handleChange(e, 'iban')}
            type={"number"}
            className="text-input"
          />
          <button
            className={"right-button"}
            type={"submit"}
          >
            next
          </button>
          <button
            className={"left-button"}
            onClick={() => this.props.history.push('/')}
          >
            Main page
          </button>
        </form>
      </div>
    )
  }
}

export default connect(state => ({ dataForm: state.dataForm }))(PaymentInfo)
