import {combineReducers} from 'redux';
import dataFormReducer from './dataFormReducer';

const combinedReducers = combineReducers({
  dataForm: dataFormReducer,
});

export default combinedReducers;