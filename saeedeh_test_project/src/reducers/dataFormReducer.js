import { FORM_ACTION_TYPES } from '../actions/dataFormAction';

const storedData = localStorage.getItem('WM_dataForm')
const initialState = storedData ? JSON.parse(storedData) :
{
  firstName: '',
  lastName: '',
  phoneNumber: '',
  street: '',
  houseNumber: '',
  zipCode: '',
  city: '',
  accountOwner: '',
  iban: '',
  paymentDataId: null
}

export default (state = initialState, action) => {

    switch (action.type) {
        case FORM_ACTION_TYPES.CHANGE_VALUE:
            return Object.assign({}, state, {
                // If we use [key, value] in payload, then it's easy to have less lines of codes
                [action.payload.key] : action.payload.value
            });
        
        default:
          return state
    }
}