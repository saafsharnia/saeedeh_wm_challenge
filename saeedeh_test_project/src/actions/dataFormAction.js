import Enum from 'es6-enum';

export const FORM_ACTION_TYPES = Enum('CHANGE_VALUE');

export function setdataForm(key, value) {
  return dispatch => {
    dispatch({
      type: FORM_ACTION_TYPES.CHANGE_VALUE,
      payload: {
        key: key,
        value: value
      }
    })
  }
}

export function submitData(data) {
  // const url = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';
  const url = '/wunderfleet-recruiting-backend-dev-save-payment-data.json'
  return dispatch => {
    return fetch(url, 
    {
      // method: 'POST',
      method: 'GET'
      // mode: 'no-cors', // no-cors is not needed for internal json fetch
      // body: JSON.stringify(data), // body is not required in get method
    }
    )
    .then(response => response.json())
    .then(data => {
      // to add paymentDataId to our localstorage in browser
      const WM_dataForm = localStorage.getItem('WM_dataForm');
      let dataForm = WM_dataForm ? JSON.parse(WM_dataForm) : {};
      dataForm['paymentDataId'] = data.paymentDataId
      localStorage.setItem('WM_dataForm', JSON.stringify(dataForm));
      dispatch({
        type: FORM_ACTION_TYPES.CHANGE_VALUE,
        payload: {
          key: 'paymentDataId',
          value: data.paymentDataId
        }
      })
    })
    .catch(error => {
      console.log(error)
      alert('something went wrong, please try again later')
    })
  }
}
